import { ProductModule } from './../../models/product/product.module';
import { ProductService } from './../../services/product.service';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Product } from 'src/app/models/product';

@Component({
  selector: 'node-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {
  products!: Product[];
  usersId: any;
  productSub!: Subscription;
  loading!: boolean;

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
   this.productSub = this.productService.products$.subscribe(
    (products: Product[])=>{
      this.loading = true;
      this.products = products;

    },
    (err)=>{
      this.loading= false;
      console.log(err);

    }

   );

   this.productService.getProducts();


  }

  ngOnDestroy(): void{
    this.productSub.unsubscribe();
  }

}
