import { AuthService } from './../../../services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'node-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isAuth!: boolean;

  constructor(private auth: AuthService) { }

  ngOnInit(): void {
    this.isAuth.isAuth$.this.service.function
      .subscribe((bool: boolean) => this.isAuth = bool);

  }

  logout(){
    this.auth.logout();
  }

}
