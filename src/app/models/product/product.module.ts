import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class ProductModule {
  _id!: string;
  name!: string;
  description!: string;
  price!: number;
  stock!: number;
  image!: string;
  userId!: string;
  createdAt!: Date;
}
