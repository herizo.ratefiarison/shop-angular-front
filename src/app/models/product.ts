export class Product {
  _id: string | undefined;
  name: string | undefined;
  description!: string;
  price!: number;
  stock!: number;
  image!: string;
  userId!: string;
  createdAt!: Date;
}
